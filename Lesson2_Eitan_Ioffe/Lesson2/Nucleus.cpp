#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}

void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
--------------------------------------------------------------------------------
*/

void Nucleus::init(const std::string dna_sequence)
{
	this->_complementary_DNA_strand = "";

	for (unsigned int i = 0; i < dna_sequence.length(); i++)
	{
		if (!(dna_sequence[i] == 'A' || dna_sequence[i] == 'C' || dna_sequence[i] == 'G' || dna_sequence[i] == 'T')) // if the DNA is illegal
		{
			std::cerr << "DNA is illegal";
			_exit(1);
		}

		// Creating the complementary DNA strand
		if (dna_sequence[i] == 'A')
		{
			this->_complementary_DNA_strand += 'T';
		}
		else if (dna_sequence[i] == 'C')
		{
			this->_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else
		{
			this->_complementary_DNA_strand += 'A';
		}
	}

	this->_DNA_strand = dna_sequence;
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string dna_strand = "";
	std::string rna_transcript = "";
	if (gene.is_on_complementary_dna_strand())
	{
		dna_strand = this->_complementary_DNA_strand;
	}
	else
	{
		dna_strand = this->_DNA_strand;
	}

	for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (dna_strand[i] == 'T')
		{
			rna_transcript += 'U';
		}
		else
		{
			rna_transcript += dna_strand[i];
		}
	}

	return rna_transcript;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string rev_dna = this->_DNA_strand; // copying dna
	std::reverse(rev_dna.begin(), rev_dna.end()); // reversing string

	return rev_dna;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int count = 0;
	int pos = -1;

	while ((pos = this->_DNA_strand.find(codon, pos + 1)) != -1)
	{
		count++;
	}

	return count;
}