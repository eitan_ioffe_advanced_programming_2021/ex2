#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	bool has_glocuse_receptor = false;
	AminoAcidNode* curr = protein.get_first();
	
	// nested conditions because it is easer to see it that way
	// Checking that the linked list contains this values in this orded
	if (curr->get_data() == ALANINE)
	{
		if (curr->get_next()->get_data() == LEUCINE)
		{
			if (curr->get_next()->get_next()->get_data() == GLYCINE)
			{
				if (curr->get_next()->get_next()->get_next()->get_data() == HISTIDINE)
				{
					if (curr->get_next()->get_next()->get_next()->get_next()->get_data() == LEUCINE)
					{
						if (curr->get_next()->get_next()->get_next()->get_next()->get_next()->get_data() == PHENYLALANINE)
						{
							if (curr->get_next()->get_next()->get_next()->get_next()->get_next()->get_next()->get_data() == AMINO_CHAIN_END)
							{
								has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}

	this->_has_glocuse_receptor = has_glocuse_receptor;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	bool atp_produce = false;
	if (this->_has_glocuse_receptor == true && this->_glocuse_level >= 50)
	{
		atp_produce = true;
	}

	return atp_produce;
}