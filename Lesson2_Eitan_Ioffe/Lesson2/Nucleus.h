#ifndef NUCLEUS_H
#define NUCLEUS_H

#include <iostream>

class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	/* function initializes the fields
	input: start, end, on_complementary_dna_strand
	output: none*/
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	// getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	// setters
	void setStart(const unsigned int start);
	void setEnd(const unsigned int end);
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
};


class Nucleus
{
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
public:
	/* function initializes the fields and checks if the input is leggal
	input: dna sequence
	output: none*/
	void init(const std::string dna_sequence);
	/* function creats the RNA transcript from the DNA strand and the gene
	input: the gene
	output: RNA transcript*/
	std::string get_RNA_transcript(const Gene& gene) const;
	/* function returns the reversed DNA strand
	input: none
	output: the reversed DNA strand*/
	std::string get_reversed_DNA_strand() const;
	/* function returns the number of codon appearances
	input: the codon (referance)
	output: the number of codon appearances*/
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
};

#endif /* NUCLEUS_H */