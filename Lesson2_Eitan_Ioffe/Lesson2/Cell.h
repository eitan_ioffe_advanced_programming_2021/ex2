#ifndef CELL_H
#define CELL_H

#include <iostream>
#include "Nucleus.h"
#include "Protein.h"
#include "Ribosome.h"
#include "AminoAcid.h"
#include "Mitochondrion.h"

#define GLUCOSE_LEVEL 50
#define ATP_UNITS 100

class Cell
{
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
public:
	/*Intializes the cell
	input: dna sequence, glucose receptor gene  | output: none*/
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);

	/*Function loads the cell with energy
	input: none  |  output: none*/
	bool get_ATP();
};

#endif