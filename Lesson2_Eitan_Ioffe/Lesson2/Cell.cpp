#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	// initializing (exept ribosome)
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_atp_units = 0;
}

bool Cell::get_ATP()
{
	bool success = true;
	Protein* protein = nullptr;
	// getting the transcript
	std::string transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);

	// creating the protein
	protein = this->_ribosome.create_protein(transcript);

	// sending protein to the mitochondrion
	this->_mitochondrion.insert_glucose_receptor(*protein);
	this->_mitochondrion.set_glucose(GLUCOSE_LEVEL); // setting the glucose level to 50 
	// Producing the atp
	success = this->_mitochondrion.produceATP();
	if (success)
	{
		this->_atp_units = ATP_UNITS; 
	}

	return success;
}
