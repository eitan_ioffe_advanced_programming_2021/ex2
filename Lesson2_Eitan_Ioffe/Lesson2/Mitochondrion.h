#ifndef MITOCHONDRION_H
#define MITOCHONDRION_H

#include <iostream>
#include "Protein.h"

class Mitochondrion
{
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
public:
	/*Initializes the fields
	input: none | output: none*/
	void init();

	/*Checks if the Protein is in the right format
	input: the protein | output: none*/
	void insert_glucose_receptor(const Protein& protein);

	// _glocuse_level setter
	void set_glucose(const unsigned int glocuse_units);

	/*Checks if the Mitochondrion can produce atp
	input: none	  |   output: true if can produce, no if else*/
	bool produceATP() const;
};

#endif
