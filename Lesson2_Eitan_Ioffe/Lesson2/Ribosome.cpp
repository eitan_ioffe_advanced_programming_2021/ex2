#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	int pos = 0;
	std::string cut = "";

	Protein* protein = new Protein; // allocating memory
	protein->init();

	while ((cut = RNA_transcript.substr(pos, NUC_TO_CUT)).length() == NUC_TO_CUT) // cutting the 3 nucleotides every time
	{
		if (get_amino_acid(cut) == UNKNOWN) // Checking if the amino acid is good
		{
			protein->clear();
			delete (protein);
			protein = nullptr;
			break;
		}
		else
		{
			protein->add(get_amino_acid(cut)); // adding acid to the list
		}
		pos += NUC_TO_CUT;
	}

	return protein;
}