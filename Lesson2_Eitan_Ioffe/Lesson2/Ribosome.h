#ifndef RIBOSOME_H
#define RIBOSOME_H

#include <iostream>
#include "Protein.h"
#include "AminoAcid.h"

#define NUC_TO_CUT 3

class Ribosome
{
public:
	/*
	Function returns a Protein based on the RNA sent
	input: the RNA transcript
	output: the protein
	*/
	Protein* create_protein(std::string& RNA_transcript) const;
};

#endif